<?php
   include_once 'head.php';
   include_once 'includes/dbh.inc.php';

?>


<body>
  
  <header>
    
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top" >
    <div class="container">
      <a class="navbar-brand" href="index.php" >myArticle</a>
      <ul class="navbar-nav">
        <li class="nav-item"><a class="nav-link " href="index.php">Home</a></li>
        <li class="nav-item"><a class="nav-link" href="trending.php">Trending</a></li>
        <li class="nav-item"><a class="nav-link" href="write.php">Write Article</a></li>
      </ul>
      <?php

      if(isset($_SESSION['u_id'])){
        
        echo '<form action="includes/logout.inc.php" method="POST">
                <button type="submit" name="submit">Logout</button>
              </form>';
      }
      else{
        echo '<form action="includes/login.inc.php" method="POST">
                <input type="text" name="uid" placeholder="username/email">
                <input type="password" name="password" placeholder="password">
                <button type="submit" name="submit">Login</button>
                <button><a href="signup.php" style=" text-decoration: none; color:black">Sign Up</a></button>
              </form>';
      }

      ?>
    </div>
  </nav>
  </header>

  <section>
      <div class="container">
          <?php
            $article_id = $_GET['id'];
            $sql = "select * from articles where article_id='$article_id';";
            $result = mysqli_query($conn, $sql);
            $row = mysqli_fetch_assoc($result);
            $writer = $row['writer_id'];
            $sql = "select * from users where user_id='$writer'";
            $result = mysqli_query($conn, $sql);
            $user_row = mysqli_fetch_assoc($result);
            $views = $row['views'] + 1;
            $sql = "update articles set views='$views' where article_id='$article_id';";
            mysqli_query($conn, $sql);
            echo "<br><h2 align=center> ".$row['article_title']."</h2><br>";
            echo "<h5 align=center>Written by: ".$user_row['user_first_name']." ".$user_row['user_last_name']."</h5>";
            echo "<p>".$row['article_content']."</p>";
          ?>
      </div>
  </section>
  <section>
      <div class="container">
          <h4><u>comments!</u></h4>
            <div>
              <form action="includes/comment.inc.php?id=<?php echo $_GET['id'];?>" method="POST">
                <textarea class="md-textarea form-control" style="height:10%; width:80%"  name="comment" ></textarea>
                <button type="submit" name="submit"> post comment</button>
              </form>
          </div>
          <?php
            $sql = "select * from comments;";
            $result = mysqli_query($conn, $sql);
            while($row = mysqli_fetch_assoc($result)){
              echo '<br>';
              echo '<li class="list-group-item" style="max-height:80px; overflow:hidden;border:none; ">';
              echo '<div>';
              $user_id = $row['comment_writer'];
              $sql = "select * from users where user_id='$user_id';";
              $resultnew = mysqli_query($conn, $sql);
              $user = mysqli_fetch_assoc($resultnew);
              echo "<h5 >".$user['user_first_name']." ".$user['user_last_name']." </h2>";
              echo "<p>".$row['comment_content']." </p>";
              echo "</div>";
              echo '</li>';
              echo "";
            }

          ?>
          
      </div>
  </section>