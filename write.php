<?php
   include_once 'head.php';
?>

<body>
  
  <header>
    
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top" >
    <div class="container">
      <a class="navbar-brand" href="index.php" >paidArticle</a>
      <ul class="navbar-nav">
        <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
        <li class="nav-item"><a class="nav-link" href="trending.php">Trending</a></li>
        <li class="nav-item"><a class="nav-link active" href="write.php">Write Article</a></li>
      </ul>
      <?php

      if(isset($_SESSION['u_id'])){
        
        echo '<form action="includes/logout.inc.php" method="POST">
                <button type="submit" name="submit">Logout</button>
              </form>';
      }
      elseif(!isset($_SESSION['u_id'])){
        header("Location: signup.php");
        exit();
      }
      ?>
      </div>
    </nav>
    </header>

    <section>
    <div class="container"  >
      <form action="includes/write.inc.php" method="POST">
        
        <div align=center >
          <input name="title" class="biggest_size" contenteditable="true" placeholder="Enter Title.."><hr>
          <input name="topic" class="bigger_size" contenteditable="true" placeholder="Enter the topic to which the article is related..."><br>
        </div>
        <br>
        <div>
          <textarea class="md-textarea form-control" style="height:70%;"  name="content" >
            
          </textarea>
        </div>
        <button type="submit" name="submit">Post</button>

      </form>
    </div>
    </section>
  
