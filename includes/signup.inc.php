<?php
session_start();

if(isset($_POST['submit'])){

    include_once 'dbh.inc.php';

    $first =  $_POST['first_name'];
    $last =  $_POST['last_name'];
    $user_name = $_POST['user_name'];
    $password =  $_POST['password'];
    $email =  $_POST['email'];

    $hashedPwd = password_hash($password, PASSWORD_DEFAULT);
    
    $sql = "select * from users where user_name='$user_name';";
    $result = mysqli_query($conn, $sql);
    $checkRows = mysqli_num_rows($result);
    if($checkRows > 0){
        echo "<h1>user name already taken!</h1>";
        header("Location: ../signup.php?username_not_available");
        
        exit();
    }
    else{
        $sql = "insert into users (user_first_name, user_last_name, user_email, user_password, user_name) 
                    values ('$first', '$last', '$email', '$hashedPwd', '$user_name');";
        $result = mysqli_query($conn, $sql);
        $row = mysqli_fetch_assoc($result);
        $_SESSION['u_id'] = $row['user_id'];
        $_SESSION['u_first'] = $row['user_first_name'];
        $_SESSION['u_last'] = $row['user_last_name'];
        $_SESSION['u_username'] = $row['user_name'];
        $_SESSION['u_email'] = $row['user_email'];
        header("Location: ../index.php?signup=successful");
        exit();

    }
    
}