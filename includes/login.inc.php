<?php

session_start();

if (isset($_POST['submit'])){

    include_once 'dbh.inc.php';

    $uid = $_POST['uid'];
    $pwd = $_POST['password'];

    if(empty($uid) || empty($pwd)){
        header("Location: ../index.php?login=empty");
        exit();
    }
    else{
        $sql = "select * from users where user_name='$uid' or user_email='$uid';";
        $result = mysqli_query($conn, $sql);
        $resultCheck = mysqli_num_rows($result);
        if($resultCheck <1){
            header("Location: ../index.php?login=error");
            exit();
        }
        else{
            if($row = mysqli_fetch_assoc($result)){
                $hashedPwdCheck = password_verify($pwd, $row['user_password']);
                if ($hashedPwdCheck == false){
                    header("Location: ../index.php?login=error");
                    exit();
                }
                elseif( $hashedPwdCheck == true){
                    $_SESSION['u_id'] = $row['user_id'];
                    $_SESSION['u_first'] = $row['user_first_name'];
                    $_SESSION['u_last'] = $row['user_last_name'];
                    $_SESSION['u_username'] = $row['user_name'];
                    $_SESSION['u_email'] = $row['user_email'];
                    header("Location: ../index.php?login=success");
                    exit();
                }
            }
        }

    }
}
else{
    header("Location: ../index.php?login=error");
    exit();
}