<?php
   include_once 'head.php';
   include_once 'includes/dbh.inc.php';

?>


<body>
  
  <header>
    
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top" >
    <div class="container">
      <a class="navbar-brand" href="index.php" >myArticle</a>
      <ul class="navbar-nav">
        <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
        <li class="nav-item"><a class="nav-link active" href="trending.php">Trending</a></li>
        <li class="nav-item"><a class="nav-link" href="write.php">Write Article</a></li>
      </ul>
      <?php

      if(isset($_SESSION['u_id'])){
        
        echo '<form action="includes/logout.inc.php" method="POST">
                <button type="submit" name="submit">Logout</button>
              </form>';
      }
      else{
        echo '<form action="includes/login.inc.php" method="POST">
                <input type="text" name="uid" placeholder="username/email">
                <input type="password" name="password" placeholder="password">
                <button type="submit" name="submit">Login</button>
                <button><a href="signup.php" style=" text-decoration: none; color:black">Sign Up</a></button>
              </form>';
      }

      ?>
    </div>
  </nav>
  </header>

  <section>
    <div class="container">
      <br> 
      <ul class="list-group">
      <?php
        $sql = "select * from articles order by views DESC;";
        $result = mysqli_query($conn, $sql);
        while($row = mysqli_fetch_assoc($result)){
          $article_id = $row['article_id'];
          echo '<a href="article.php?id= '.$article_id.'" style="color: black; text-decoration: none; ">';
          echo '<br>';
          echo '<li class="list-group-item" style="max-height:150px; overflow:hidden;border:none; ">';
          echo '<div>';
          echo "<h2 align=center>".$row['article_title']." </h2>";
          echo "<p>".$row['article_content']." </p>";
          echo "</div>";
          echo '</li>';
          echo "<br>";
        }
        ?>
      
      </ul>
    </div>
  </section>
</body>
</html>